var templateURL = "juju";

var main_off = new Image();
var main_over = new Image();
var main_on = new Image();

var playlist_off = new Image();
var playlist_over = new Image();
var playlist_on = new Image();

var roundups_off = new Image();
var roundups_over = new Image();
var roundups_on = new Image();

var misc_off = new Image();
var misc_over = new Image();
var misc_on = new Image();

var about_off = new Image();
var about_over = new Image();
var about_on = new Image();

var contact_off = new Image();
var contact_over = new Image();
var contact_on = new Image();

var search_off = new Image();
var search_over = new Image();
var search_on = new Image();

var send_off = new Image();
var send_over = new Image();
var send_on = new Image();


function init(url) {
	templateURL = url;
	
	main_off.src = templateURL + "/images/main_off.gif";
	main_over.src = templateURL + "/images/main_on.gif";
	main_on.src = templateURL + "/images/main_on.gif";

	playlist_off.src = templateURL + "/images/playlist_off.gif";
	playlist_over.src = templateURL + "/images/playlist_on.gif";
	playlist_on.src = templateURL + "/images/playlist_on.gif";

	roundups_off.src = templateURL + "/images/roundups_off.gif";
	roundups_over.src = templateURL + "/images/roundups_on.gif";
	roundups_on.src = templateURL + "/images/roundups_on.gif";

	misc_off.src = templateURL + "/images/misc_off.gif";
	misc_over.src = templateURL + "/images/misc_on.gif";
	misc_on.src = templateURL + "/images/misc_on.gif";

	about_off.src = templateURL + "/images/about_off.gif";
	about_over.src = templateURL + "/images/about_on.gif";
	about_on.src = templateURL + "/images/about_on.gif";

	contact_off.src = templateURL + "/images/contact_off.gif";
	contact_over.src = templateURL + "/images/contact_on.gif";
	contact_on.src = templateURL + "/images/contact_on.gif";

	search_off.src = templateURL + "/images/search_off.gif";
	search_over.src = templateURL + "/images/search_on.gif";
	search_on.src = templateURL + "/images/search_on.gif";

	send_off.src = templateURL + "/images/send_off.gif";
	send_over.src = templateURL + "/images/send_on.gif";
	send_on.src = templateURL + "/images/send_on.gif";

}

function img_on(imgName) {
	document[imgName].src = eval(imgName + "_on.src");
}

function img_over(imgName) {
	document[imgName].src = eval(imgName + "_on.src");
}

function img_off(imgName) {
	document[imgName].src = eval(imgName + "_off.src");
}



//no errors before the applet is done loading                    
window.onerror=null;

